module Main

import IdrisTest.TestResult
import TestChapter2

total 
allTests : List IdrisTest.TestResult.TestResult
allTests = TestChapter2.tests

total
main : IO ()
main = putStrLn $ IdrisTest.TestResult.toJsons allTests
