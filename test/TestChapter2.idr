module TestChapter2

import Chapter2
import IdrisTest.TestResult

total
testPalindromeTrue : IdrisTest.TestResult.TestResult
testPalindromeTrue = 
  if Chapter2.palindrome "racecar"
  then Pass "Chapter2" "palindrome" "racecar is palindrome."
  else Fail "Chapter2" "palindrome" "racecar should be palindrome."

total
testPalindromeFalse : IdrisTest.TestResult.TestResult
testPalindromeFalse = 
  if Chapter2.palindrome "race car"
  then Fail "Chapter2" "palindrome" "race car is not palindrome."
  else Pass "Chapter2" "palindrome" "race car should not be palindrome."
  
total 
testPalindromeOfLength : IdrisTest.TestResult.TestResult
testPalindromeOfLength = 
  if Chapter2.palindromeOfLength 10 "fleetomeremoteelf"
  then Pass "Chapter2" "palindromeOfLength" "long enough."
  else Fail "Chapter2" "palindromeOfLength" "should be long enough."

export total
tests : List IdrisTest.TestResult.TestResult
tests = [ testPalindromeTrue, testPalindromeFalse, testPalindromeOfLength ] 
