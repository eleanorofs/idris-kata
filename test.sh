#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p bash cacert git idris2

mkdir dependencies
cd dependencies
git clone https://gitlab.com/eleanorofs/idris-test.git
cd idris-test
idris2 --build idris-test.ipkg
idris2 --install idris-test.ipkg
cd ..
cd ..
idris2 --build kata.ipkg
idris2 --install kata.ipkg
idris2 --build test.ipkg
./build/exec/idris-kata-test > test-results.json
if grep "❌" test-results.json; then
    echo "TEST FAILURE";
    exit 1;
fi
