module Chapter3

total
lengthHelper : List a -> Nat -> Nat
lengthHelper Nil acc = acc
lengthHelper (x :: xs) acc = lengthHelper xs (acc + 1)

total 
length : List a -> Nat
length as = lengthHelper as 0


total 
reverseHelper : List a -> List a -> List a 
reverseHelper Nil acc = acc
reverseHelper (x :: xs) acc = reverseHelper xs (x :: acc)

total
reverse : List a -> List a 
reverse as = reverseHelper as Nil

total
mapHelper : (a -> b) -> List a -> List b -> List b
mapHelper f Nil acc = acc
mapHelper f (x :: xs) acc = mapHelper f xs ((f x) :: acc)

total
map : (a -> b) -> List a -> List b 
map f xs = mapHelper f xs Nil
