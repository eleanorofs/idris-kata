module Chapter2

import Data.String
import Data.List

||| Takes a string and returns True iff. str is a palindrome.
export total
palindrome : String -> Bool
palindrome str = str == reverse str

||| Takes a string and returns True if (case insensitive) str is a palindrome.
export total
caseInsensitivePalindrome : String -> Bool
caseInsensitivePalindrome str = palindrome $ toLower str

||| Returns True if str is a palindrome and longer than `len`
export total
palindromeOfLength : Nat -> String -> Bool
palindromeOfLength len str = length str > len && palindrome str

export
counts : String -> (Nat, Nat)
counts str = (length $ words str, length str)

export total
top_ten : Ord a => List a -> List a 
top_ten as = take 10 $ sort as

export total
over_length : Nat -> List String -> Nat
over_length n strings = length (filter (\str => length str > n ) strings)
